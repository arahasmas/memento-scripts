/**
   Library: Formazione
   Type: JS field
   Field: StringaDescrizione
   Real-time execution: yes
*/

function dateTimeToString(dateTime) {
    /**
       Create a readable date string.
       @param {dateTime} dateTime - A dateTime object
       @return {string}
       @example
       dateTimeToString(aDateTimeObject) > "15/2/1921"
    */
    return dateTime.getDate() + '/'
        + (dateTime.getMonth() + 1) + '/'
        + dateTime.getFullYear();
}

function padTwoChars(aString) {
    /**
       Take a string and return the last two chars of the same string
       left-padded with zeros.
       @param {string} aString - the string to pad
       @return {string}
       @example
       padTwoChars("3") > "03"
    */
    return ("00" + aString).slice(-2);
}

function linkStr (link) {
		/**
       Takes a field and return the string (actually a little symbol)
       to be showed as a description of the link to the event.
       @param {field object} link - the field "Link"
       @return {string}
       @example
       linkStr(field("Link"));
    */
    switch (link) {
        case "In attesa":
            return " 📪";
        case "Email":
            return " 📧";
        case "Riconosco":
            return " 🔰";
        case "Altro":
            return "";
    }
}

function inizioStr (inizio) {
		/**
       Takes a field and return the string describing date and time of
       the event beginning in the form WWW dd mmmm yyyy hh:mm.
       @param {field object} inizio - the field "Inizio"
       @return {string}
       @example
       inizioStr(field("Link"));
    */
		let weekDays = ["Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab"];
    let weekDay = weekDays[inizio.getDay()];
    let inizioData = dateTimeToString(inizio);
    let inizioOre = inizio.getHours() + ":" + padTwoChars(inizio.getMinutes());
    return weekDay + " " + inizioData + " " + inizioOre;
}

function durataStr (durata) {
		/**
       Takes a field and return the string describing the duration of
       the event in the form hh:mm.
       @param {field object} durata - the field "Durata"
       @return {string}
       @example
       durataStr(field("Link"));
    */
		let durataOre = Math.trunc(durata / 3600000);
    let durataMinuti = Math.trunc((durata - (durataOre * 3600000)) / 60000);
    return durataOre + ":" + padTwoChars(durataMinuti);
}

function sessioniStr (sessione, sessioni) {
		/**
       Takes two fields and return the string describing the nymber of
       sessions and the particular session.
       @param {field object} sessione - the field "Sessione"
			 @param {field object} sessioni - the field "Sessioni"
       @return {string}
       @example
       sessioniStr(field("Link"));
    */
		let result = "";
    if ((sessione != null) &&
        (sessione != "") &&
        (sessioni != null) &&
        (sessioni != "")) {
        result = "\n" + sessione + " di " + sessioni;
    }
		return result;
}

function eventStr (inizio, durata, sessione, sessioni, link) {
    /**
       Takes some fields value and return the string to be showed as a
       description if the value of the field "Tipo" is "Evento".
       @param {field object} inizio - the field "Inizio"
       @param {field object} durata - the field "Durata"
       @param {field object} sessione - the field "Sessione"
       @param {field object} sessioni - the field "Sessioni"
			 @param {field object} link - the field "Link"
       @return {string}
       @example
       eventStr(field("Inizio"), field("Durata"),
                field("Sessione"), field("Sessioni"),
								field("Link"));
    */
    return inizioStr(inizio) + " | " + durataStr(durata) +
				sessioniStr(sessione, sessioni) + linkStr(link);
}

function exemptionStr (fromDate, toDate) {
    /**
       Takes some fields value and return the string to be showed as a
       description if the value of the field "Tipo" is "Esonero".
       @param {field object} fromDate - the field "Dal"
       @param {field object} toDate - the field "Al"
       @return {string}
       @example
       exemptionStr(field("Dal"), field("Al"));
    */
    let dataInizio = dateTimeToString(fromDate);
    let dataFine = dateTimeToString(toDate);
    return "Dal " + dataInizio + " al " + dataFine;
}

/**
   Here we calculate the value of the field.
   If the value of the field "Tipo" is "Evento" create the string

     Lun 11/1/2020 14.30 | 2.30
     1 di 4

   else if the value of the field "Tipo" is "Esonero" create the string

     Dal 1/9/2019 al 31/12/2019
*/
let isEvent = (field("Tipo") == "Evento");

if (isEvent) {
    eventStr(field("Inizio"), field("Durata"),
             field("Sessione"), field("Sessioni"), field("Link"));
} else {
    exemptionStr(field("Dal"), field("Al"))
}

// End of file StringaDescrizione.js
