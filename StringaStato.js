/*
   Library: Formazione
   Type: JS field
   Field: StringaStato
   Real-time execution: yes
*/

/**
   If the value of the field "Tipo" is "Evento" create the string

     Ord: 2 Obbl: 1

   else if the value of the field "Tipo" is "Esonero" simply return
   the value of the field "Esito".
*/

let isEvent = (field("Tipo") == "Evento");
if (isEvent) {
    let ordinari = field("Ordinari");
    let obbligatori = field("Obbligatori");
    let strOrdinari = "";
    let strObbligatori = "";
    let space = "";

		if ((ordinari !== null) && (ordinari !== "")) {
      strOrdinari = "Ord: ";
    } else {
      ordinari = "";
    }

		if ((obbligatori !== null) && (obbligatori !== "")) {
      strObbligatori = "Obbl: ";
      space = " ";
    } else {
      obbligatori = "";
    }

		if ((ordinari == "") || (obbligatori == "")) {
      space = "";
    }

		strOrdinari + ordinari + space + strObbligatori + obbligatori;
  } else {
    field("Esito");
  }

// End of file StringaStato.js
